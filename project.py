# -*- encoding: utf-8 -*-
"""
PyMap Project

Holds catalogs and results.

"""

from PyQt4 import QtCore
from quakepy.QPCatalogCompact import QPCatalogCompact


class Project(QtCore.QObject):

    # Signals
    catalogs_changed = QtCore.pyqtSignal()

    def __init__(self):
        super(Project, self).__init__()
        self.catalogs = []

    def import_catalog(self, catalog_file):
        """
        Load a QuakePy compact catalog from a file

        :param catalog_file: catalog file name (path)
        :type catalog_file: str

        """
        catalog = QPCatalogCompact()
        catalog.read(catalog_file)
        self.catalogs.append(catalog)
        self.catalogs_changed.emit()
