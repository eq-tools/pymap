# -*- encoding: utf-8 -*-
"""
Toplevel application object

Bootstraps the application and ties everything together (more specifically
the GUI and the Atls core application).

"""

import sys
import os
import logging

from PyQt4 import QtGui, QtCore
from qgis.core import QgsApplication
from ui.pymapgui import PyMapGui
from project import Project


VERSION = '0.1 "Bug Infested Alpha"'

# Initialize QGIS
QgsApplication.setPrefixPath(os.environ['QGIS_PREFIX'], True)


class PyMap(QtCore.QObject):
    """
    Top level application object

    Emits the app_launched signal as soon as the program has started (i.e.
    as soon as the event loop becomes active)

    """

    # Signals
    app_launched = QtCore.pyqtSignal()
    project_loaded = QtCore.pyqtSignal()

    def __init__(self, args):
        """
        Instantiates PyMap and the Qt app (run loop) and wires the GUI.

        :param args: command line arguments that were provided by the user
        :type args: dict

        """
        super(PyMap, self).__init__()
        self.thread().setObjectName('Main')
        # Setup the logger
        self.logger = logging.getLogger(__name__)
        self.logger.info('Launching PyMap v' + VERSION)
        # Instantiate the appropriate Qt app object
        self.has_gui = not args.no_gui
        if self.has_gui:
            self.qt_app = QtGui.QApplication(sys.argv)
            # Initialize QGis Libraries
            QgsApplication.initQgis()
        else:
            self.qt_app = QtCore.QCoreApplication(sys.argv)
        # Register general app information
        self.qt_app.setApplicationName('PyMap')
        self.qt_app.setOrganizationDomain('sed.ethz.ch')
        self.qt_app.setApplicationVersion(VERSION)
        self.qt_app.setOrganizationName('SED')
        # Launch gui
        if self.has_gui:
            self.gui = PyMapGui(self)
        # Connect essential signals
        self.app_launched.connect(self.on_app_launched)
        self.qt_app.aboutToQuit.connect(self.on_quit)
        # Setup components
        # FIXME: we should probably not start with an empty project
        self.project = Project()
        self.project_loaded.emit()

    def run(self):
        """
        Launches the PyMap application

        The app launch signal will be delivered as soon as the event loop
        starts (which happens in exec_(). This function does not return until
        the app exits.

        """
        self.logger.info('PyMap is ready')

        if self.has_gui:
            self.gui.show()
        QtCore.QTimer.singleShot(0, self._emit_app_launched)
        sys.exit(self.qt_app.exec_())

    # Core Methods

    # def load_catalog(self, catalog_file):
    #     """
    #     Load a QuakePy compact catalog from a file

    #     :param catalog_file: catalog file name (path)
    #     :type catalog_file: str

    #     """
    #     catalog = QPCatalogCompact()
    #     catalog.read(catalog_file)
    #     self.catalog = catalog

    # Signal slots

    def on_quit(self):
        if self.has_gui:
            QgsApplication.exitQgis()

    def on_app_launched(self):
        # Check if we should load a project on launch
        pass

    def _emit_app_launched(self):
        self.app_launched.emit()
