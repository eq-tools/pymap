#!/usr/bin/env bash

# This bootstrap scripts sets up the dependencies for PyMap development
# on an ubuntu trusty (14.04) machine.
#
# Note that while you might use this script to set up your own machine,
# it's original intention was to be used in combination with vagrant,
# i.e. it mostly assumes that the target machine is empty at the time
# the script is executed.

# dependencies for PyMap / QuakePy
DEB_PACKAGES="python-qt4 qgis subversion git python-dev python-pip python-lxml python-shapely"
PIP_PACKAGES="numpy pyrxp egenix-mx-base geopy pyproj"

# source and target path for quakepy
QP_URL="https://quake.ethz.ch/quakepy/GettingQuakePy?action=AttachFile&do=get&target=quakepy-0.4.tgz"
QP_DST="/usr/local/lib/python2.7/dist-packages/quakepy-0.4/quakepy"

# make sure we get the latest qgis from qgis.org
add-apt-repository "deb     http://qgis.org/debian trusty main"

# install deb and pip packages
apt-get update
apt-get install -y --force-yes $DEB_PACKAGES
pip install $PIP_PACKAGES

# set QGIS_PREFIX=/usr for all users if it isn't already (required for pygis to work)
if [ -z "$QGIS_PREFIX" ]; then
	echo "export QGIS_PREFIX=/usr" >> /etc/profile.d/qgis_prefix.sh
fi

# install quakepy 0.4
if [ ! -d $QP_DST ]; then
	wget -q --no-check-certificate -O quakepy-0.4.tgz $QP_URL
	mkdir -p $QP_DST
	tar xzf quakepy-0.4.tgz --strip-components=1 -C $QP_DST

	# we need to monkey patch quakepy since the pyRXP lib has been renamed to pyRXPU
	# after quakepy 0.4 was released
	find $QP_DST -name "*.py" -exec sed -i 's/pyRXP/pyRXPU/g' {} \+

	echo $(dirname "$QP_DST") >> /usr/local/lib/python2.7/dist-packages/quakepy.pth
fi
