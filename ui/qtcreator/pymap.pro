#-------------------------------------------------
#
# Project created by QtCreator 2015-04-01T17:15:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pymap
TEMPLATE = app


SOURCES +=

HEADERS  +=

FORMS += \
    ../mainwindow.ui \
    mainwindow.ui
