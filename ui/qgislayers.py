# -*- encoding: utf-8 -*-
"""
Short Description

Long Description

Copyright (C) 2013, ETH Zurich - Swiss Seismological Service SED

"""

from PyQt4.QtCore import QVariant, Qt
from qgis.core import QgsVectorLayer, QgsField, QgsFeature, QgsGeometry, \
    QgsPoint


class PyMapDataPointsLayer(QgsVectorLayer):
    """
    Generic layer to display data points

    """
    def __init__(self, name):
        super(PyMapDataPointsLayer, self).__init__('Point', name, 'memory')


class PyMapCatalogLayer(PyMapDataPointsLayer):

    def __init__(self, name):
        super(PyMapCatalogLayer, self).__init__(name)

        pr = self.dataProvider()
        pr.addAttributes([QgsField('magnitude', QVariant.Double)])
        self.updateFields()
        symbol = self.rendererV2().symbol()
        symbol.setColor(Qt.red)
        # Make the dot size depend on the magnitude (with an arbitrary scale
        # factor)
        symbol.symbolLayer(0).setDataDefinedProperty('size',
                                                     '"magnitude" / 6.0 * 10')

    def set_catalog(self, catalog):
        """
        Set catalog to display on the layer

        :param catalog: QP Compact Catalog
        :type catalog: QPCatalogCompact

        """
        pr = self.dataProvider()
        self.startEditing()
        pr.deleteFeatures(self.allFeatureIds())

        # Turn catalog rows into QGis features
        mag_idx = catalog.map['mag']
        lat_idx = catalog.map['lat']
        lon_idx = catalog.map['lon']

        def to_feature(row):
            location = QgsPoint(row[lon_idx], row[lat_idx])
            magnitude = float(row[mag_idx])
            f = QgsFeature()
            f.setGeometry(QgsGeometry.fromPoint(location))
            f.setAttributes([magnitude])
            return f

        features = [to_feature(row) for row in catalog.catalog]

        pr.addFeatures(features)
        self.commitChanges()
        self.updateExtents()

    def clear(self):
        self.startEditing()
        self.dataProvider().deleteFeatures(self.allFeatureIds())
        self.commitChanges()
