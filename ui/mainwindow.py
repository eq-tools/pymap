# -*- encoding: utf-8 -*-
"""
Controller module for the main window

TODO: abstract background (and other) layers into a map object

"""

import os
import logging

from PyQt4 import QtGui, uic
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QColor
from qgis.core import QgsVectorLayer, QgsMapLayerRegistry, QgsRectangle, \
    QgsCoordinateReferenceSystem
from qgis.gui import QgsMapCanvasLayer
from qgislayers import PyMapCatalogLayer


# Translate and import the QtCreator form dynamically
ui_path = os.path.dirname(__file__)
main_window_path = os.path.join(ui_path, 'qtcreator', 'mainwindow.ui')
Ui_MainWindow = uic.loadUiType(main_window_path)[0]

# Shape files for background map
BG_LAYER_PATH = 'resources/background_layers'
BG_LAYER_FILES = ['ne_10m_ocean.shp',
                  'ne_10m_admin_0_boundary_lines_land.shp',
                  'ne_10m_lakes.shp',
                  'ne_10m_land.shp']
# Shape colors
LAYER_COLORS = [QColor.fromRgb(37, 52, 148),    # ocean
                QColor.fromRgb(30, 30, 30),     # borders
                QColor.fromRgb(65, 182, 196),   # lakes
                QColor.fromRgb(250, 250, 250)]  # land


# Create a class for our main window
class MainWindow(QtGui.QMainWindow):
    """
    Class that manages the main application window

    """

    def __init__(self, pymap):
        QtGui.QMainWindow.__init__(self)
        self.logger = logging.getLogger(__name__)

        # Other windows which are lazy-loaded
        # ...

        # Keep a reference to the core (business logic)
        self.pymap = pymap

        # Setup the user interface
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.mapPresenter = MapPresenter(self.ui)

        # Hook up the menu
        self.ui.actionImport_Catalog.triggered.\
            connect(self.action_import_catalog)

        # Hook up essential signals from the core and the forecast core
        pymap.app_launched.connect(self.on_app_launch)
        pymap.project_loaded.connect(self.on_project_load)

    # Qt Signal Slots

    def on_app_launch(self):
        # enable menus, refresh ui if required
        pass

    def on_project_load(self):
        # FIXME: we also need to disconnect before the previous project
        # unloads
        self.pymap.project.catalogs_changed.connect(self.on_catalog_change)

    def on_catalog_change(self):
        self.mapPresenter.clear_catalogs()
        for catalog in self.pymap.project.catalogs:
            self.mapPresenter.show_catalog(catalog)

    # Menu Actions

    def action_import_catalog(self):
        if not self.pymap.project:
            return
        home = os.path.expanduser("~")
        path = QtGui.QFileDialog.getOpenFileName(None,
                                                 'Import Catalog',
                                                 home,
                                                 'QP Compact Catalog (*)')
        self.pymap.project.import_catalog(path)


class MapPresenter(object):
    """
    Handles the Map content

    """

    def __init__(self, ui):
        """
        :param ui: reference to the Qt UI
        :type ui: Ui_MainWindow

        """
        self.logger = logging.getLogger(__name__)

        self.ui = ui
        self.ui.mapWidget.setCanvasColor(Qt.white)

        # Add background map layers
        shape_files = [os.path.join(BG_LAYER_PATH, shp_file)
                       for shp_file in BG_LAYER_FILES]
        self.background_layers = []
        for shp_file, color in zip(shape_files, LAYER_COLORS):
            self.logger.info('loading {}'.format(shp_file))
            path = os.path.abspath(os.path.join(shp_file))
            layer = QgsVectorLayer(path, "shp_file", "ogr")
            if not layer.isValid():
                self.logger.error('Layer at {} failed to load!'.format(path))
            else:
                symbols = layer.rendererV2().symbols()
                symbol = symbols[0]
                symbol.setColor(color)
                self.background_layers.append(layer)

        # Work in the standard CRS (WGS-84) and reproject all other layers to
        # the target CRS on the fly
        crs = QgsCoordinateReferenceSystem("EPSG:4326")
        self.ui.mapWidget.setDestinationCrs(crs)
        self.ui.mapWidget.setCrsTransformEnabled(True)

        # Add catalog layer (events)
        self.catalog_layers = []

        # add layer to the registry
        for layer in self.background_layers:
            QgsMapLayerRegistry.instance().addMapLayer(layer)

        # set extent to the extent of our layer
        # TODO: hard coding this is probably not a good idea
        self.ui.mapWidget.setExtent(QgsRectangle(5.6, 45.6, 10.8, 47.5))
        self._update_canvas_layers()

    def _update_canvas_layers(self):
        all_layers = self.catalog_layers + self.background_layers
        canvas_layers = [QgsMapCanvasLayer(l) for l in all_layers]
        self.ui.mapWidget.setLayerSet(canvas_layers)

    def clear_catalogs(self):
        """
        Clear all catalogs from the map

        """
        layer_ids = [l.id() for l in self.catalog_layers]
        QgsMapLayerRegistry.instance().removeMapLayers(layer_ids)
        self.catalog_layers = []
        self._update_canvas_layers

    def show_catalog(self, catalog):
        """
        Display all events from a QP compact catalog

        :param catalog: QuakePy compact catalog
        :type: QPCatalogCompact

        """
        self.logger.info('displaying catalog')

        catalog_layer = PyMapCatalogLayer('Catalog')
        catalog_layer.set_catalog(catalog)

        QgsMapLayerRegistry.instance().addMapLayer(catalog_layer)
        self.catalog_layers.append(catalog_layer)
        self._update_canvas_layers()

        # Auto adjust the view rectangle
        extent = catalog_layer.extent()
        extent.scale(1.1)
        self.ui.mapWidget.setExtent(extent)
