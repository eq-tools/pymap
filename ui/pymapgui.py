# -*- encoding: utf-8 -*-
"""
Graphical user interface for PyMap

"""


import logging
from ui.mainwindow import MainWindow

RESOURCES_PATH = 'resources'


class PyMapGui:

    def __init__(self, pymap):
        """
        :param pymap: reference to the pymap core

        """
        self.main_window = MainWindow(pymap)
        self._logger = logging.getLogger(__name__)

    def show(self):
        self.main_window.show()
