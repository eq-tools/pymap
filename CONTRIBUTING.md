# Requirements

This guide collects the requirements for PyMap and defines the architecture,
interfaces and core classes.


## Guiding Principles

- PyMap is built first and foremost as a library. It doesn't make assumptions
whether it's used in a console, playbook script or GUI environment.
- A GUI for PyMap shall be built later. It will most likely be Qt based. The 
library itself should thus not provide interactive elements. It should provide basic 
plotting using basemap and matplotlib but visual output to the screen should 
always be optional (i.e. give an option to return the figure to the caller in memory).
- PyMap should be light on dependencies particularly ones that are difficult to
install or don't coexist well with other common software packages in the 
seismic domain, such as QuakeMl, obspy, openquake (due to e.g. version
conflicts).
- We don't reinvent the wheel if we don't have to. When other libraries provide
functions we only reimplement them if we have a good reason.
- PyMap must support catalogs at different scales and with different 
coordinate representations. Specifically 'natural' catalogs with lat, lon, depth
coordinates and local cartesian coordinates with x, y, z coordinates and 
(optionally) a reference point.
- Documentation is important. We intend to use sphinx to auto-generate
documentation from code comments.
- The [obspy coding style guide](http://docs.obspy.org/coding_style.html) applies.


## Software Architecture

- PyMap provides a catalog object that holds a list of events in a flat format
and methods to import and export a simple common catalog from a file
(QPCompactCatalog from QuakePy). All catalog conversion from/to different formats
will be performed by other 
tools such as QuakePy or ObsPy for now. Since the internal catalog is flat 
(a pandas array), we don't support alternative origins etc. on the same event. 
- Processing functions and classes shall be grouped into thematical modules and
shall all accept the catalog object as input and provide catalog objects as
output where applicable.


## Catalog Format

Most if not all tools in the seismic domain assume geographical coordinates for
the catalogs they process. In PyMap we want to support geographical coordinates
but also local cartesian coordinates. For example to analyse events in a rock
lab experiment or induced seismicity from a geothermal project users will
typically work with local cartesian coordinates.

To support this, any catalog instance will be one of two concrete
implementations of an abstract **Catalog** base class:

1. A GlobalCatalog (name TBD) which stores coordinates as *lat, lon, depth*
2. A LocalCatalog (name TBD) which stores coordinates as *x, y, z* together
with a reference point in *lat, lon, depth*. The base vectors for *x, y, z*
use the cartographic convention of *x* pointing north, *y* pointing east and
*z* pointing to down.

Units are degrees (or rad?) and meters respectively.

All concrete implementations of the **Catalog** class implement methods to
return cartesian or spherical coordinates. These coordinate conversions
happen on the fly to avoid storing redundant information. Processing functions
will call the appropriate method to get the coordinates they need. 

E.g.

    def distance_to_point(catalog, point):
        # get cartesian coordinates
        x, y, z = catalog.cartesian()
        # compute distances
        # ...

### Import/Export of Catalogs

As mentioned above, PyMap will implement only one import/export function to the
simplyfied catalog QPCompactCatalog of QuakyPy. Ideally, PyMap will use QuakePy
to allow PyMap users to import a variety of catalog formats. 

The QPCompactCatalog will use panda arrays so that each earthquake is stored in
one row. All columns need to be attributed so that re-imports to QuakePy are possible
(at least for the standard columns). The user should be able to extend the rows with 
extra columns (that will be ognored by the standard PyMap functions). However,
they enable the user to use these columns for other computations using the PyMap
library.

## Requirements

Collect methods to implement here, going from basic functions to more 
complex higher level calculations.

### Basic Spatial Computations

- distance to a point (cartesian)
- distance to a point along great circle? (cartographic)

### Event Selection and Filtering
- filter events based on time, magnitude, depth, and spatially
- select events within surface polygon and optional depth (cartographic)
- select events within 3D bounding box (cartesian)
- select events within a circle (cylinder considering depth)
- spatial binning with surface grid
- spatial binning in 3D
- magnitude binning

### Basic Catalog Statistics

- compute b-value (various approaches)
- compute GR-relation parameters (a- & b-value and associated uncertainties)
- compute Mc using
    - Maximum Curvature method
    - Goodness-of-fit method
    - EMR method
- use completeness values provided by the PMC method
- compute Omori-law parameters

### Catalog Modifications

- Create synthetic catalogs for testing purposes (select a-, b, Mc-values)
- Bootstrap catalogs
- Decluster catalog using Gardner & Knopoff [1974] and Reasenberg [1983]
 
### Advanced Analyses

- Analyze magnitude shifts
- Analyze GR-parameter errors

### Gridding

- Create spatial 2D/3D grids and compute parameters for each node
- Create 2d cross-sections
- Create cross-sections along fault traces

### Plotting

- plot catalog on map
- plot catalog cross section
- plot catalog in 3D?


### Statistic Catalog Comparison

TBD it might make sense to include some of the tests applied in 

> Kiraly-Proag, E., J. D. Zechar, V. Gischig, S. Wiemer, D. Karvounis, and J. Doetsch (2016), Validating induced seismicity forecast models—Induced Seismicity Test Bench, J. Geophys. Res. Solid Earth, 121, 6009–6029

as long as they are general enough to fit within the scope of PyMap. I.e. we probably don't want to do forecast model tests but general statistical catalog comparisons could be useful.

- log likelihood based spatial tests
- log likelihood based number tests 

