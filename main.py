# -*- encoding: utf-8 -*-
"""
Main file

The Main file sets up the user interface and bootstraps the application

"""

import argparse
import logging

# We use API v2 for Qt objects, since they make working with variants easier
# and are more future proof (v2 is default in python 3).
# This needs to be done before we import PyQt4
import sip
sip.setapi(u'QDate', 2)
sip.setapi(u'QDateTime', 2)
sip.setapi(u'QString', 2)
sip.setapi(u'QTextStream', 2)
sip.setapi(u'QTime', 2)
sip.setapi(u'QUrl', 2)
sip.setapi(u'QVariant', 2)

from pymap import PyMap


def main():
    """
    Launches PyMap

    Creates the PyMap top level object and passes control to it.

    """

    parser = argparse.ArgumentParser(description='PyMap')
    parser.add_argument('-n', '--no-gui', action='store_true',
                        help='runs PyMap without a GUI')
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        default=1, help="output verbosity (0-2, default 0)")

    args = parser.parse_args()

    configure_logging(args.verbosity)
    pymap = PyMap(args)
    pymap.run()


def configure_logging(verbosity):
    """
    Configures the root logger.

    All loggers in submodules will automatically become children of the root
    logger and inherit some of the properties.

    """
    lvl_lookup = {
        0: logging.WARN,
        1: logging.INFO,
        2: logging.DEBUG
    }
    root_logger = logging.getLogger()
    root_logger.setLevel(lvl_lookup[verbosity])
    formatter = logging.Formatter('%(asctime)s %(levelname)s: '
                                  '[%(name)s] %(message)s')
    # ...handlers from 3rd party modules - we don't like your kind here
    for h in list(root_logger.handlers):
        root_logger.removeHandler(h)
    # ...setup console logging
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    root_logger.addHandler(console_handler)


if __name__ == "__main__":
    main()
