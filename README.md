[![build status](https://gitlab.seismo.ethz.ch/ci/projects/2/status.png?ref=master)](https://gitlab.seismo.ethz.ch/ci/projects/2?ref=master)

# PyMap

PyMap is a library for basic earthquake catalog
statistics. A GUI will be added later. 

We're just getting started, stay tuned.

To contribute, please refer to the [contribution guide](https://gitlab.seismo.ethz.ch/eq-tools/pymap/blob/master/CONTRIBUTING.md)